# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/telmate/proxmox" {
  version     = "2.9.11"
  constraints = "2.9.11"
  hashes = [
    "h1:RKM2pvHNJrQKcMD7omaPiM099vWGgDnnZqn1kGknYXU=",
  ]
}
