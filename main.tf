terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.11"
    }
  }
}

provider "proxmox" {
  pm_api_url = "https://${var.pve_ip}:8006/api2/json"
  pm_api_token_id = var.api_token_id
  pm_api_token_secret = var.api_token
  pm_tls_insecure = true
}