variable "proxmox_api_url" {
    type = string
}

variable "proxmox_api_token_id" {
    type = string
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
}

# Resource Definiation for the VM Template
source "proxmox-iso" "proxmox-centos-8" {
 
    # Proxmox Connection Settings
    proxmox_url = "${var.proxmox_api_url}"
    username = "${var.proxmox_api_token_id}"
    token = "${var.proxmox_api_token_secret}"
    insecure_skip_tls_verify = true
    
    # VM General Settings
    node = "pve1" # proxmox node name
    vm_id = "600"
    vm_name = "centos-8-server"
    template_description = "Test CentOS8"

    # VM OS Settings
    # Local ISO File
    iso_file = "local:iso/CentOS-Stream-8-x86_64-latest-dvd1.iso"
    iso_storage_pool = "local"
    # (Option) Download ISO. If there is no local iso file you can comment "iso_file" string and uncomment "iso_url".
    #iso_url = "http://mirror.surf/centos/8-stream/isos/x86_64/CentOS-Stream-8-x86_64-latest-boot.iso"
    #iso_checksum = "70030af1dff1aed857e9a53311b452d330fa82902b3567f6640f119f9fa29e70"

    unmount_iso = true

    # VM System Settings
    qemu_agent = true

    # VM Hard Disk Settings
    scsi_controller = "virtio-scsi-pci"

    disks {
        disk_size = "10G"
        format = "raw"
        storage_pool = "local-1TB-ZFS"
        storage_pool_type = "zfs"
        type = "virtio"
    }

    # VM CPU Settings
    cores = "4"
    
    # VM Memory Settings
    memory = "2048" 

    # VM Network Settings
    network_adapters {
        model = "virtio"
        bridge = "vmbr0"
        firewall = "false"
    } 

    # VM Cloud-Init Settings
    cloud_init = true
    cloud_init_storage_pool = "local-zfs"

    # PACKER Boot Commands
    boot_command = [
        "<tab> text ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/cent8.inst.cfg<enter><wait>"
    ]
    boot = "c"
    boot_wait = "5s"

    # PACKER Autoinstall Settings
    http_directory = "http" 
    # (Optional if have web-server with kickstarter conf) Bind IP Address and Port
    #http_bind_address = "192.168.88.254"
    #http_port_min = 3003
    #http_port_max = 3003

    ssh_username = "root"
    ssh_private_key_file = "~/.ssh/nasavushkin"

    # Raise the timeout, when installation takes longer
    ssh_timeout = "30m"
}

# Build Definition to create the VM Template
build {

    name = "proxmox-centos-8-repo-srv"
    sources = ["source.proxmox-iso.proxmox-centos-8"]

    # Upd kernel and Provisioning the VM Template for Cloud-Init Integration in Proxmox #1
    provisioner "shell" {
        inline = [
            "";
            ""
        ]
    }

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #2
    provisioner "file" {
        source = "files/99-pve.cfg"
        destination = "/tmp/99-pve.cfg"
    }

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #3
    provisioner "shell" {
        inline = [ "sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg" ]
    }

}