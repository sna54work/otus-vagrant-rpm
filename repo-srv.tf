resource "proxmox_vm_qemu" "repo-srv" {
  count = 1
  name = "CentOS8-repo-srv"
  target_node = var.proxmox_host
  vmid = var.vmid.master
  clone = var.template_name
  agent = 1
  os_type = "cloud-init"
  cores = var.cores.master
  sockets = var.sockets.master
  cpu = "host"
  memory = var.memory.master
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  pool = var.pool
  
  disk {
    slot = 0
    size = var.disk.master
    type = "scsi"
    storage = var.pve_storage
    iothread = 1
  }
  
  network {
    model = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  ipconfig0 = "ip=${var.net.ip}/${var.net.mask},gw=${var.net.gw}"
  sshkeys = <<EOF
  ${var.ssh_pubkey}
  EOF

    provisioner "local-exec" {
      command = "echo 'Copy repo install script to server'"
  }
    provisioner "file" {
      source      = "scripts/repo-srv-install.sh"
      destination = "/home/${var.ssh_user}/repo-srv-install.sh"
    connection {
      type        = "ssh"
      user        = var.ssh_user
      host        = var.net.ip
      private_key = "${file("${var.ssh_priv_key}")}"
    }
  }
}