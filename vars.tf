variable "ssh_pubkey" {
type = string
}

variable "api_token_id" {
  type = string
}

variable "proxmox_host" {
    type = string
}

variable "template_name" {
    type = string
}

variable "pve_storage" {
  type = string
}

variable "pool" {
  type = string
}

variable "vmid" {
  type = object({
    master = string
  })
}

variable "ssh_priv_key" {
  type = string
}

variable "api_token" {
  type = string
}

variable "pve_ip" {
  type = string
}

variable "disk" {
  type = object({
    master = string
  })
}

variable "cores" {
  type = object({
    master = string
  })
}

variable "sockets" {
  type = object({
    master = string
  })
}

variable "memory" {
  type = object({
    master = string
  })
}

variable "net" {
  type = object({
    ip = string
    mask = string
    gw = string
  })
}

variable "ssh_user" {
  type = string
}