#!/bin/bash

# Install the necessary packages
yum install -y createrepo httpd

# Create the repository directory
mkdir -p /srv/repo/{RPMS,SRPMS}

# Copy the RPM packages to the appropriate directories
#cp /path/to/rpms/*.rpm /srv/repo/RPMS/
#cp /path/to/srpms/*.rpm /srv/repo/SRPMS/

# Create the repository metadata
createrepo /srv/repo/

# Create a symbolic link from the repository to the Apache document root
ln -s /srv/repo /var/www/html/repo

# Set the correct permissions for the repository directory
chown -R apache:apache /srv/repo

# Start the Apache service
systemctl start httpd

# Enable the Apache service to start automatically on boot
systemctl enable httpd